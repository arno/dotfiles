cd ~/.vim/bundle
for bundle in "$(pwd)/"*; do
  echo "update ${bundle}"
  cd "${bundle}"
  git pull
  cd ..
done
